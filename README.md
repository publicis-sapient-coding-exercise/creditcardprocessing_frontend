# Credit Card Processing Frontend

The Credit Card Processing frontend offers a Angular web UI where you can view existing accounts and add a new account.

# Prerequisites
*  Node Package Manager (npm) installed
*  npm install -g @angular/cli@1.7.4

# Setup
1.  Checkout project
2.  Open terminal and navigate to the project folder
3.  Build project `ng build`
4.  Run tests `ng test`

# Start Application

Run `ng serve --open`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
