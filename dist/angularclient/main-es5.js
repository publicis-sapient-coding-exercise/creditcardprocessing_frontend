(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<div *ngIf=\"globalError != empty\" class=\"global-error alert alert-danger center container-fluid\">{{ globalError }}</div>\n\n<div class=\"container\" style=\"text-align:left\">\n  <h1>{{ title }}</h1>\n</div>\n\n<div class=\"accounts container\">\n\n  <div class=\"add-accounts\">\n\n    <h2 id=\"addAccountsTitle\">{{ addAccountsTitle }}</h2>\n\n    <form (ngSubmit)=\"onSubmit(accountForm)\" id=\"accountForm\" #accountForm=\"ngForm\">\n      <div>\n        <label for=\"name\">Name</label>\n        <input type=\"text\"\n               id=\"name\"\n               name=\"name\"\n               class=\"form-control\"\n               [disabled]=\"!serverReady\"\n               [(ngModel)]=\"accountRequest.accountHolderName\"\n               #accountHolderName=\"ngModel\"\n               required/>\n\n        <div [hidden]=\"accountHolderName.valid || accountHolderName.pristine\" class=\"alert alert-danger\">Name is required</div>\n        <div *ngIf=\"nameError != empty\" class=\"alert alert-danger\">{{ nameError }}</div>\n      </div>\n\n      <div>\n        <label for=\"cardNumber\">Card Number</label>\n        <input type=\"text\"\n               id=\"cardNumber\"\n               name=\"cardNumber\"\n               class=\"form-control ng-pristine ng-invalid\"\n               pattern=\"^[0-9()\\-+\\s]+$\"\n               maxlength=\"19\"\n               [disabled]=\"!serverReady\"\n               [(ngModel)]=\"accountRequest.cardNumber\"\n               #cardNumber=\"ngModel\" required/>\n        <div [hidden]=\"cardNumber.valid || cardNumber.pristine\" class=\"alert alert-danger\">Card Number is required</div>\n        <div *ngIf=\"cardNumberError != empty\" class=\"alert alert-danger\">{{ cardNumberError }}</div>\n      </div>\n\n      <div>\n        <label for=\"cardLimit\">Limit</label>\n        <input type=\"number\"\n               id=\"cardLimit\"\n               name=\"cardLimit\"\n               class=\"form-control\"\n               [disabled]=\"!serverReady\"\n               [(ngModel)]=\"accountRequest.cardLimit\"\n               #cardLimit=\"ngModel\"\n               required/>\n        <div [hidden]=\"cardLimit.valid || cardLimit.pristine\" class=\"alert alert-danger\">Limit is required</div>\n        <div *ngIf=\"limitError != empty\" class=\"alert alert-danger\">{{ limitError }}</div>\n      </div>\n\n      <div>\n        <button type=\"submit\" [disabled]=\"!accountForm.form.valid || !serverReady\" class=\"btn btn-info\">Add</button>\n      </div>\n    </form>\n  </div>\n\n  <div class=\"list-of-accounts\">\n\n    <h2 id=\"existingAccountsTitle\">{{ existingAccountsTitle }}</h2>\n\n    <table class=\"table\">\n      <thead>\n      <tr>\n        <th scope=\"col\">Name</th>\n        <th scope=\"col\">Card Number</th>\n        <th scope=\"col\">Balance</th>\n        <th scope=\"col\">Limit</th>\n      </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let account of accounts\">\n          <td>{{ account.accountHolderName }}</td>\n          <td>{{ account.card.cardNumber }}</td>\n\n          <td *ngIf=\"account.card.balance == 'DEBIT'\" class=\"debit\">\n            -{{ account.card.amount }}\n          </td>\n\n          <td *ngIf=\"account.card.balance == 'CREDIT' || account.card.balance == null\">\n          {{ account.card.amount }}\n          </td>\n\n          <td>{{ account.card.limit }}</td>\n        </tr>\n      </tbody>\n    </table>\n\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.debit { color: red; }\n\ntable { border-collapse: collapse; text-align: center;}\n\ntable, th, tr, td { border: 1px solid rgba(86,61,124,.50) }\n\nth { background-color: rgba(86,61,124,.15); }\n\ndiv.add-accounts     {margin-top: 50px; width: 50%}\n\ndiv.list-of-accounts {margin-top: 50px;}\n\ninput[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0;\n}\n\ninput {\n  -webkit-transition: all 0.30s ease-in-out;\n  -moz-transition: all 0.30s ease-in-out;\n  -ms-transition: all 0.30s ease-in-out;\n  -o-transition: all 0.30s ease-in-out;\n  outline: none;\n  padding: 3px 0px 3px 3px;\n  margin: 5px 1px 3px 0px;\n  border: 1px solid #DDDDDD;\n}\n\ninput:focus {\n  box-shadow: 0 0 5px rgba(81, 203, 238, 1);\n  padding: 3px 0px 3px 3px;\n  margin: 5px 1px 3px 0px;\n  border: 1px solid rgba(81, 203, 238, 1);\n}\n\ndiv.add-accounts form div {margin-top: 30px;}\n\nbutton[disabled] { background-color: grey; border: darkgray; }\n\n.global-error {text-align: center; font-weight: bold;}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLFNBQVMsVUFBVSxFQUFFOztBQUVyQixRQUFRLHlCQUF5QixFQUFFLGtCQUFrQixDQUFDOztBQUN0RCxvQkFBb0Isc0NBQXNDOztBQUMxRCxLQUFLLHFDQUFxQyxFQUFFOztBQUU1QyxzQkFBc0IsZ0JBQWdCLEVBQUUsVUFBVTs7QUFDbEQsc0JBQXNCLGdCQUFnQixDQUFDOztBQUV2Qzs7RUFFRSx3QkFBd0I7RUFDeEIsU0FBUztBQUNYOztBQUVBO0VBQ0UseUNBQXlDO0VBQ3pDLHNDQUFzQztFQUN0QyxxQ0FBcUM7RUFDckMsb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYix3QkFBd0I7RUFDeEIsdUJBQXVCO0VBQ3ZCLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLHlDQUF5QztFQUN6Qyx3QkFBd0I7RUFDeEIsdUJBQXVCO0VBQ3ZCLHVDQUF1QztBQUN6Qzs7QUFFQSwyQkFBMkIsZ0JBQWdCLENBQUM7O0FBRTVDLG1CQUFtQixzQkFBc0IsRUFBRSxnQkFBZ0IsRUFBRTs7QUFFN0QsZUFBZSxrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uZGViaXQgeyBjb2xvcjogcmVkOyB9XG5cbnRhYmxlIHsgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgdGV4dC1hbGlnbjogY2VudGVyO31cbnRhYmxlLCB0aCwgdHIsIHRkIHsgYm9yZGVyOiAxcHggc29saWQgcmdiYSg4Niw2MSwxMjQsLjUwKSB9XG50aCB7IGJhY2tncm91bmQtY29sb3I6IHJnYmEoODYsNjEsMTI0LC4xNSk7IH1cblxuZGl2LmFkZC1hY2NvdW50cyAgICAge21hcmdpbi10b3A6IDUwcHg7IHdpZHRoOiA1MCV9XG5kaXYubGlzdC1vZi1hY2NvdW50cyB7bWFyZ2luLXRvcDogNTBweDt9XG5cbmlucHV0W3R5cGU9bnVtYmVyXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixcbmlucHV0W3R5cGU9bnVtYmVyXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xufVxuXG5pbnB1dCB7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMzBzIGVhc2UtaW4tb3V0O1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjMwcyBlYXNlLWluLW91dDtcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjMwcyBlYXNlLWluLW91dDtcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMzBzIGVhc2UtaW4tb3V0O1xuICBvdXRsaW5lOiBub25lO1xuICBwYWRkaW5nOiAzcHggMHB4IDNweCAzcHg7XG4gIG1hcmdpbjogNXB4IDFweCAzcHggMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjREREREREO1xufVxuXG5pbnB1dDpmb2N1cyB7XG4gIGJveC1zaGFkb3c6IDAgMCA1cHggcmdiYSg4MSwgMjAzLCAyMzgsIDEpO1xuICBwYWRkaW5nOiAzcHggMHB4IDNweCAzcHg7XG4gIG1hcmdpbjogNXB4IDFweCAzcHggMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDgxLCAyMDMsIDIzOCwgMSk7XG59XG5cbmRpdi5hZGQtYWNjb3VudHMgZm9ybSBkaXYge21hcmdpbi10b3A6IDMwcHg7fVxuXG5idXR0b25bZGlzYWJsZWRdIHsgYmFja2dyb3VuZC1jb2xvcjogZ3JleTsgYm9yZGVyOiBkYXJrZ3JheTsgfVxuXG4uZ2xvYmFsLWVycm9yIHt0ZXh0LWFsaWduOiBjZW50ZXI7IGZvbnQtd2VpZ2h0OiBib2xkO31cbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./model/card */ "./src/app/model/card.ts");
/* harmony import */ var _model_account__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./model/account */ "./src/app/model/account.ts");
/* harmony import */ var _model_account_request__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./model/account-request */ "./src/app/model/account-request.ts");
/* harmony import */ var _service_accounts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service/accounts-service */ "./src/app/service/accounts-service.ts");






var AppComponent = /** @class */ (function () {
    function AppComponent(accountsService) {
        this.accountsService = accountsService;
        this.title = 'Credit Card Processing';
        this.addAccountsTitle = 'Add';
        this.existingAccountsTitle = 'Existing Cards';
        this.accountRequest = new _model_account_request__WEBPACK_IMPORTED_MODULE_4__["AccountRequest"]();
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accountsService.findAll().subscribe(function (data) {
            var listOfAccounts = [];
            data.forEach(function (acc) {
                acc.cards.forEach(function (card) {
                    var formattedCardNumber = card.cardNumber.toString().match(/.{1,4}/g).join(' ');
                    listOfAccounts.push(new _model_account__WEBPACK_IMPORTED_MODULE_3__["Account"](acc.accountHolderName, new _model_card__WEBPACK_IMPORTED_MODULE_2__["Card"](formattedCardNumber, card.balance, card.amount, card.limit)));
                });
            });
            _this.accounts = listOfAccounts;
            _this.serverReady = true;
        }, function (error) {
            _this.serverReady = false;
            _this.globalError = error;
        });
    };
    AppComponent.prototype.onSubmit = function (accountForm) {
        var _this = this;
        this.resetErrors();
        this.accountsService.save(this.accountRequest).subscribe(function (result) {
            accountForm.reset();
            _this.reloadPage();
        }, function (error) {
            if (error.errors != null) {
                error.errors.forEach(function (error) {
                    switch (error.field) {
                        case 'accountHolderName': {
                            this.nameError = error.message;
                            break;
                        }
                        case 'cardNumber': {
                            this.cardNumberError = error.message;
                            break;
                        }
                        case 'cardLimit': {
                            this.limitError = error.message;
                            break;
                        }
                        default: {
                            this.globalError = "Server Unavailable. Please try again later.";
                            break;
                        }
                    }
                }.bind(_this));
            }
            else {
                _this.globalError = error;
            }
            _this.reloadPage();
        });
    };
    AppComponent.prototype.reloadPage = function () {
        this.ngOnInit();
    };
    AppComponent.prototype.resetErrors = function () {
        this.nameError = null;
        this.cardNumberError = null;
        this.limitError = null;
        this.globalError = null;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_accounts_service__WEBPACK_IMPORTED_MODULE_5__["AccountsService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _service_accounts_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/accounts-service */ "./src/app/service/accounts-service.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            providers: [_service_accounts_service__WEBPACK_IMPORTED_MODULE_6__["AccountsService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/model/account-request.ts":
/*!******************************************!*\
  !*** ./src/app/model/account-request.ts ***!
  \******************************************/
/*! exports provided: AccountRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountRequest", function() { return AccountRequest; });
var AccountRequest = /** @class */ (function () {
    function AccountRequest() {
    }
    return AccountRequest;
}());



/***/ }),

/***/ "./src/app/model/account.ts":
/*!**********************************!*\
  !*** ./src/app/model/account.ts ***!
  \**********************************/
/*! exports provided: Account */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Account", function() { return Account; });
var Account = /** @class */ (function () {
    function Account(accountHolderName, card) {
        this.accountHolderName = accountHolderName;
        this.card = card;
    }
    return Account;
}());



/***/ }),

/***/ "./src/app/model/card.ts":
/*!*******************************!*\
  !*** ./src/app/model/card.ts ***!
  \*******************************/
/*! exports provided: Card */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Card", function() { return Card; });
var Card = /** @class */ (function () {
    function Card(cardNumber, balance, amount, limit) {
        this.cardNumber = cardNumber;
        this.balance = balance;
        this.amount = amount;
        this.limit = limit;
    }
    return Card;
}());



/***/ }),

/***/ "./src/app/service/accounts-service.ts":
/*!*********************************************!*\
  !*** ./src/app/service/accounts-service.ts ***!
  \*********************************************/
/*! exports provided: AccountsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsService", function() { return AccountsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");





var AccountsService = /** @class */ (function () {
    function AccountsService(http) {
        this.http = http;
        this.accountUrl = 'http://localhost:8080/accounts';
    }
    AccountsService.prototype.findAll = function () {
        return this.http.get(this.accountUrl, { headers: { 'Content-Type': 'application/json' } })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    AccountsService.prototype.save = function (accountRequest) {
        return this.http.post(this.accountUrl, JSON.stringify(accountRequest), { headers: { 'Content-Type': 'application/json' } })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    AccountsService.prototype.errorHandler = function (errorResponse) {
        if (errorResponse.status == 0) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])("Server Unavailable. Please try again later.");
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(errorResponse.error);
    };
    AccountsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AccountsService);
    return AccountsService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/hsembhi/Documents/Workspaces/IntelliJ/Exercises/PublicisSapient_CreditCardProcessingApplication/creditcardprocessing_frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map