import { Component, OnInit } from '@angular/core';
import { Card } from './model/card';
import { Account } from './model/account';
import { AccountRequest } from './model/account-request';
import { AccountsService } from './service/accounts-service';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title: string;
  addAccountsTitle: string;
  existingAccountsTitle: string;
  globalError: string;
  nameError: string;
  cardNumberError: string;
  limitError: string;
  serverReady: boolean;
  accounts: Account[];
  accountRequest: AccountRequest;

  constructor(private accountsService: AccountsService) {
    this.title = 'Credit Card Processing';
    this.addAccountsTitle = 'Add';
    this.existingAccountsTitle = 'Existing Cards';
    this.accountRequest = new AccountRequest();
  }

  ngOnInit() {
    this.accountsService.findAll().subscribe(
          data => {
                      let listOfAccounts: Account[] = [];

                      data.forEach(function(acc) {
                        acc.cards.forEach(function(card) {
                          var formattedCardNumber = card.cardNumber.toString().match(/.{1,4}/g).join(' ');
                          listOfAccounts.push(new Account(acc.accountHolderName, new Card(formattedCardNumber, card.balance, card.amount, card.limit)));
                        });
                      });
                      this.accounts = listOfAccounts;
                      this.serverReady = true;
                  },
          error => {
                      this.serverReady = false;
                      this.globalError = error;
                   }
          );
  }

  onSubmit(accountForm) {
    this.resetErrors();
    this.accountsService.save(this.accountRequest).subscribe(
          result => {
                      accountForm.reset();
                      this.reloadPage()
                    },
          error => {
                      if(error.errors != null) {
                          error.errors.forEach(function (error) {
                              switch(error.field) {
                                 case 'accountHolderName': {
                                    this.nameError = error.message;
                                    break;
                                 }
                                 case 'cardNumber': {
                                    this.cardNumberError = error.message;
                                    break;
                                 }
                                 case 'cardLimit': {
                                    this.limitError = error.message;
                                    break;
                                 }
                                 default: {
                                    this.globalError = "Server Unavailable. Please try again later.";
                                    break;
                                 }
                              }
                          }.bind(this));
                      }else{
                        this.globalError = error;
                      }
                      this.reloadPage();
                   }
    );
  }

  private reloadPage() {
    this.ngOnInit();
  }

  private resetErrors() {
    this.nameError = null;
    this.cardNumberError = null;
    this.limitError = null;
    this.globalError = null;
  }

}
