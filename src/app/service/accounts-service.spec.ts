import { TestBed, inject } from '@angular/core/testing';
import { AccountsService } from './accounts-service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AccountsService', () => {

  beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [AccountsService]
      })
  );

  it('should be created', () => {
      const accountsService: AccountsService = TestBed.get(AccountsService);
      expect(accountsService).toBeTruthy();
  });

  it('expects service to fetch list of accounts',
    inject([HttpTestingController, AccountsService],
        (httpMock: HttpTestingController, accountsService: AccountsService) => {

        expect(accountsService).toBeTruthy();
        expect(httpMock).toBeTruthy();

        // Call the service

        accountsService.findAll().subscribe(data => {
          expect(data[0].accountHolderName).toBe("Alice");
          expect(data[1].accountHolderName).toBe("Bob");
        });

        // Set the expectations for the HttpClient mock

        const req = httpMock.expectOne('http://localhost:8080/accounts');
        expect(req.request.method).toEqual('GET');

        // Set mock data

        req.flush([
                    {"accountHolderName":"Alice","cards":[{"cardNumber":"1111 2222 3333 4444","balance":"DEBIT","amount":1000.00,"limit":2000.00}]},
                    {"accountHolderName":"Bob","cards":[{"cardNumber":"5555 6666 7777 8888","balance":"DEBIT","amount":2000.00,"limit":3000.00}]}
                  ]);
      })
  );

});
