import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AccountsResponse } from '../model/accounts-response';
import { AccountRequest } from '../model/account-request';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class AccountsService {

  private accountUrl: string;

  constructor(private http: HttpClient) {
    this.accountUrl = 'http://localhost:8080/accounts';
  }

  public findAll(): Observable<AccountsResponse[]> {
    return this.http.get<AccountsResponse[]>(this.accountUrl, {headers: { 'Content-Type': 'application/json' }})
                    .pipe(catchError(this.errorHandler));
  }

  public save(accountRequest: AccountRequest) {
    return this.http.post<AccountRequest>(this.accountUrl, JSON.stringify(accountRequest), {headers: { 'Content-Type': 'application/json' }})
                    .pipe(catchError(this.errorHandler));
  }

  private errorHandler(errorResponse){
    if(errorResponse.status == 0){
      return throwError("Server Unavailable. Please try again later.");
    }
    return throwError(errorResponse.error);
  }
}
