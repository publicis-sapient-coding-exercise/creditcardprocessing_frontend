import { Card } from './card';

export class AccountsResponse {
  accountHolderName: string;
  cards: Card[];
}
