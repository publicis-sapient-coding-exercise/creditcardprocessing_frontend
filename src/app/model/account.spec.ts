import { Account } from './account';
import { Card } from './card';

describe('Account', () => {
  it('should create an instance', () => {
    expect(new Account("Alice", new Card("4111111111111111", "CREDIT", 0, 2000))).toBeTruthy();
  });
});
