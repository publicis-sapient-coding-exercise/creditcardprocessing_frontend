import { Card } from './card';

export class Account {

  accountHolderName: string;
  card: Card;

  constructor(accountHolderName: string, card: Card) {
    this.accountHolderName = accountHolderName;
    this.card = card;
  }
}
