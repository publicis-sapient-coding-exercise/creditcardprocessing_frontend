export class Card {

  cardNumber: string;
  balance: string;
  amount: number;
  limit: number;

  constructor(cardNumber: string, balance: string, amount: number, limit: number) {
    this.cardNumber = cardNumber;
    this.balance = balance;
    this.amount = amount;
    this.limit = limit;
  }
}
